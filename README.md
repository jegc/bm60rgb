# Configuration for BM60 RGB mechanical keyboard

## Install QMK cli

Install dependencies
```sh
apt install python3-virtualenv gcc-avr gcc-arm-none-eabi avrdude dfu-util libusb-dev
```

Create a virtualenv for qmk
```sh
python3 -m virtualenv qmk
```

Activate qmk virtualenv
```sh
. qmk/bin/activate
```

Install qmk
```sh
python3 -m pip install -r requirements.txt
```

## Setup QMK environment

Clone or update the qmk firmware repository

```sh
git clone --recurse-submodules https://github.com/qmk/qmk_firmware.git
or
cd $PATH_TO/qmk_firmware
git pull --recurse-submodules
```

finish setup environment

```sh
cp $PATH_TO/qmk_firmware/util/udev/50-qmk.rules /etc/udev/rules.d/
export QMK_HOME=$PATH_TO/qmk_firmware
qmk setup
```

Test environment
```sh
qmk compile -kb bm60rgb -km default
```

## Custom keypam

Create a new keymap

```sh
qmk new-keymap -kb bm60rgb -km jegc
Ψ jegc2 keymap directory created in: $QMK_HOME/keyboards/bm60rgb/keymaps/jegc
```

Edit the `keymap.c` generated in the new directory with the name of your new keymap

Compile the new keymap
```sh
qmk compile -kb bm60rgb -km jegc
```

## Flash your Keyboard

Put your keyboard in DFU (bootloader) mode pressing the physical RESET button located on the underside of the
keyboard. You'll see in your dmesg something like:

```sh
[ 8087.321564] usb 1-6: USB disconnect, device number 34
[ 8087.930739] usb 1-6: new full-speed USB device number 35 using xhci_hcd
[ 8088.085808] usb 1-6: New USB device found, idVendor=03eb, idProduct=2ff4, bcdDevice= 0.00
[ 8088.085816] usb 1-6: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[ 8088.085821] usb 1-6: Product: ATm32U4DFU
[ 8088.085824] usb 1-6: Manufacturer: ATMEL
[ 8088.085827] usb 1-6: SerialNumber: 1.0.0
```

Flash the keyboard
```sh
qmk flash -kb bm60rgb -km jegc
```
